package com.hiqfood.hiqaddin;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.Map;
import java.util.zip.GZIPOutputStream;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.hiqfood.navision.ro.Item;

/**
 * Servlet implementation class ItemsServlet
 */
@WebServlet("/Items")
public class ItemsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemsServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			long filter = Long.parseLong(request.getParameter("filter"));
			boolean zip = Boolean.parseBoolean(request.getParameter("zip"));
			boolean bypassCache = Boolean.parseBoolean(request.getParameter("bypassCache"));
			boolean asArray = Boolean.parseBoolean(request.getParameter("asArray"));
			Map<String, Item> items = Item.getItems(filter, bypassCache);
			String itemsJson;
			if (asArray) {
				itemsJson = new Gson().toJson(items.values().toArray());
			} else {
				itemsJson = new Gson().toJson(items);
			}
			if (zip) {
				ByteArrayInputStream ips = new ByteArrayInputStream(itemsJson.getBytes(StandardCharsets.UTF_8));
				ServletOutputStream out = response.getOutputStream();
				GZIPOutputStream ops = new GZIPOutputStream(out);
				int ch = 0;	while ((ch = ips.read()) != -1) ops.write(ch);
				ips.close(); ops.close(); out.close();
			} else {
				response.setContentType("application/json; charset=UTF-8");
				response.setCharacterEncoding("UTF-8");
				PrintWriter out = response.getWriter();
				out.print(itemsJson);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}	
	}
}