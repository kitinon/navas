package com.hiqfood.hiqaddin;

import java.util.List;

public class RMCalcInput {
    private String itemNo_;
    private String unit;
    private List<Double> quantities;
    
	public String getItemNo_() {
		return itemNo_;
	}
	public void setItemNo_(String itemNo_) {
		this.itemNo_ = itemNo_;
	}
	
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	
	public List<Double> getQuantities() {
		return quantities;
	}
	public void setQuantities(List<Double> quantities) {
		this.quantities = quantities;
	}
}