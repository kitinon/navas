package com.hiqfood.hiqaddin;

public class InventoriesInput {
	private String itemNo_;
	private String locationCode;
	
	public String getItemNo_() {
		return itemNo_;
	}
	public void setItemNo_(String itemNo_) {
		this.itemNo_ = itemNo_;
	}
	
	public String getLocationCode() {
		return locationCode;
	}
	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}
}
