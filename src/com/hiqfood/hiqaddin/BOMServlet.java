package com.hiqfood.hiqaddin;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.hiqfood.navision.ro.BOMLine;

/**
 * Servlet implementation class BOMServlet
 */
@WebServlet("/BOM")
public class BOMServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BOMServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			String itemNo_ = request.getParameter("itemNo_");
			boolean bypassCache = Boolean.parseBoolean(request.getParameter("bypassCache"));
			boolean simple = Boolean.parseBoolean(request.getParameter("simple"));
			List<BOMLine> bom = (simple) ? BOMLine.getSimpleBOM(itemNo_, bypassCache) : BOMLine.getBOM(itemNo_, bypassCache);
			response.setContentType("text/html; charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			PrintWriter out = response.getWriter();
			out.print(new Gson().toJson(bom));
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
}