package com.hiqfood.hiqaddin;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.hiqfood.navision.ro.Item;

/**
 * Servlet implementation class Item
 */
@WebServlet("/Item")
public class ItemServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			String itemNo_ = request.getParameter("No_");
			boolean bypassCache = Boolean.parseBoolean(request.getParameter("bypassCache"));
			Item item = Item.getItem(itemNo_, bypassCache);
			response.setContentType("text/html; charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			PrintWriter out = response.getWriter();
			out.print(new Gson().toJson(item));
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
}
