package com.hiqfood.hiqaddin;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import com.hiqfood.navision.ro.DAL;

@WebListener
public class ApplicationLifeCycleListener implements ServletContextListener {

	
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		//default is true
		//Cache.setActive(false);
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		DAL.shutdown();
	}
}
