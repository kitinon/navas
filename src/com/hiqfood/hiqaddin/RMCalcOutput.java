package com.hiqfood.hiqaddin;

import java.util.List;

public class RMCalcOutput {
    private String type; 
    private String group;
    private String code;
    private String unit;
    private String description;
    private Double unitCost; 
    private List<Double> quantities;
    
	public RMCalcOutput(String type, String group, String code, String unit,
			String description, Double unitCost, List<Double> quantities) {
		this.type = type;
		this.group = group;
		this.code = code;
		this.unit = unit;
		this.description = description;
		this.unitCost = unitCost;
		this.quantities = quantities;
	}

	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public Double getUnitCost() {
		return unitCost;
	}
	public void setUnitCost(Double unitCost) {
		this.unitCost = unitCost;
	}
	
	public List<Double> getQuantities() {
		return quantities;
	}
	public void setQuantities(List<Double> quantities) {
		this.quantities = quantities;
	}
}