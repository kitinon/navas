package com.hiqfood.hiqaddin;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hiqfood.navision.ro.BOMLine;
import com.hiqfood.navision.ro.Item;
import com.hiqfood.navision.ro.ItemBOMLine;
import com.hiqfood.navision.ro.RoutingBOMLine;
import com.hiqfood.navision.ro.UnitOfMeasure;

/**
 * Servlet implementation class RMCalcServlet
 */
@WebServlet(name = "RMCalc", urlPatterns = { "/RMCalc" })
public class RMCalcServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RMCalcServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String inputsJSON = request.getParameter("input");
		List<RMCalcOutput> outputs;
		//Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create();
		Gson gson = new Gson();
		try {
			final Type RMCalcInputsType = new TypeToken<ArrayList<RMCalcInput>>(){}.getType();
			List<RMCalcInput> inputs = gson.fromJson(inputsJSON, RMCalcInputsType);
			outputs = RMCalc(inputs);
			response.setContentType("text/html; charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			PrintWriter out = response.getWriter();
			out.print(gson.toJson(outputs));
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}

	private List<RMCalcOutput> RMCalc(List<RMCalcInput> inputs) throws ClassNotFoundException, SQLException {
		Map<String, RMCalcOutput> outputsMap = new HashMap<String, RMCalcOutput>();
		int periods = inputs.get(0).getQuantities().size();
		for (RMCalcInput input : inputs) {
			Item fgItem = Item.getItem(input.getItemNo_());
			UnitOfMeasure fgUOM = fgItem.getUnitOfMeasure(input.getUnit());
			List<BOMLine> bom = BOMLine.getSimpleBOM(input.getItemNo_());
			for (int p=0; p<periods; p++) {
				for (BOMLine bl : bom) {
					double fgQty = input.getQuantities().get(p);
					double outputQty = bl.getQuantity() * fgQty * fgUOM.getQuantityPerBase();
					RMCalcOutput output = outputsMap.get(bl.getKey());
					List<Double> outputQtys;
					if (output == null) {
						String code = bl.getKey();
						String description = bl.getDescription();
						double unitCost = bl.getStandardCost();
						outputQtys = new ArrayList<Double>();
						for (int pp=0; pp<periods; pp++) outputQtys.add((double) 0);
						if (bl instanceof ItemBOMLine) {
							Item rm = Item.getItem(bl.getKey());
							String type = rm.getType();
							String group = rm.getInventoryPostingGroup().split("-")[1];
							String unit = rm.getBaseUnit();
							output = new RMCalcOutput(type, group, code, unit, description, unitCost, outputQtys);
						} else if (bl instanceof RoutingBOMLine) {
							String type = "DL";
							String group = "LABOR";
							String unit = "�������";
							output = new RMCalcOutput(type, group, code, unit, description, unitCost, outputQtys);
						}	
						outputsMap.put(bl.getKey(), output);
					} else {
						outputQtys = output.getQuantities();
					}
					outputQtys.set(p, outputQtys.get(p) + outputQty);
				}
			}
		}
		return new ArrayList<RMCalcOutput>(outputsMap.values());
	}
}