package com.hiqfood.hiqaddin;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hiqfood.navision.ro.Inventory;

/**
 * Servlet implementation class InventoriesServlet
 */
@WebServlet(name = "Inventories", urlPatterns = { "/Inventories" })
public class InventoriesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InventoriesServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String inputsJSON = request.getParameter("input");
		List<Double> outputs;
		//Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create();
		Gson gson = new Gson();
		try {
			final Type InventoriesInputType = new TypeToken<ArrayList<InventoriesInput>>(){}.getType();
			List<InventoriesInput> inputs = gson.fromJson(inputsJSON, InventoriesInputType);
			outputs = getInventories(inputs);
			response.setContentType("text/html; charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			PrintWriter out = response.getWriter();
			out.print(gson.toJson(outputs));
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}

	private List<Double> getInventories(List<InventoriesInput> inputs) throws ClassNotFoundException, SQLException {
		List<Double> outputs = new ArrayList<Double>();
		for (InventoriesInput input : inputs) {
			String itemNo_ = input.getItemNo_();
			String locationCode = input.getLocationCode();
			Double output = null;
			Double total = null;
			List<Inventory> inventories = Inventory.getInventory(itemNo_);
			if (inventories != null) { 
				total = 0.0;
				output = 0.0;
				for (Inventory inventory : inventories) {
					total += inventory.getQuantity();
					if (inventory.getLocationCode().equals(locationCode)) {
						output = inventory.getQuantity();
						break;
					}
				}
			}
			if (locationCode == null) output = total;
			outputs.add(output);
		}
		return outputs;
	}
}
